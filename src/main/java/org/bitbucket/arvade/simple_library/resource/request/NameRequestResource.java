package org.bitbucket.arvade.simple_library.resource.request;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class NameRequestResource {

    @NotEmpty(message = "Please provide name")
    private String name;
}
