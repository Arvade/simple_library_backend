package org.bitbucket.arvade.simple_library.repository;

import org.bitbucket.arvade.simple_library.model.Book;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BookRepository extends JpaRepository<Book, Long> {

}
