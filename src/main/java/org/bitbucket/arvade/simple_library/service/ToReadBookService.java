package org.bitbucket.arvade.simple_library.service;

import org.bitbucket.arvade.simple_library.model.Book;
import org.bitbucket.arvade.simple_library.model.User;
import org.bitbucket.arvade.simple_library.resource.book.BookHomePageResource;

import java.util.List;

public interface ToReadBookService {

    void setBookAsToReadForUser(Book book, User user);

    void setBookAsToReadForUser(Long bookId, String username);

    void setBookAsNotToReadForUser(Long bookId, String username);

    boolean isBookSetToReadForUser(Long bookId, String username);

    List<BookHomePageResource> getToReadBooksForUser(String username);
}
