package org.bitbucket.arvade.simple_library.controller.admin;

import org.bitbucket.arvade.simple_library.resource.EntityIdResource;
import org.bitbucket.arvade.simple_library.service.UploadFileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@CrossOrigin("*")
public class AdminFileUploadController {

    private UploadFileService uploadFileService;

    @Autowired
    public AdminFileUploadController(UploadFileService uploadFileService) {
        this.uploadFileService = uploadFileService;
    }

    @PostMapping(value = "/app/admin/uploadFile")
    public ResponseEntity<EntityIdResource> upload(@RequestParam("file") MultipartFile file) throws IOException {
        Long id = -1L;
        if (!file.isEmpty()) {
            String fileName = file.getName();
            byte[] bytes = file.getBytes();
            id = uploadFileService.save(fileName, bytes);
        }

        return ResponseEntity.ok(new EntityIdResource(id));
    }
}
