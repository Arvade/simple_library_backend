package org.bitbucket.arvade.simple_library.controller.user;

import org.bitbucket.arvade.simple_library.resource.AdminPanelUserEntryResource;
import org.bitbucket.arvade.simple_library.resource.request.UpdateUserRequestResource;
import org.bitbucket.arvade.simple_library.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@PreAuthorize("hasRole('ADMIN')")
@CrossOrigin("*")
public class AdminUserController {

    private UserService userService;

    @Autowired
    public AdminUserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping(value = "/app/admin/users/{page}/{size}")
    public ResponseEntity getUserList(@PathVariable Integer page, @PathVariable Integer size) {
        Page<AdminPanelUserEntryResource> resultPage = userService.findUserListPaginated(page, size);
        return ResponseEntity.ok(resultPage);
    }

    @PostMapping(value = "/app/admin/user/edit")
    public ResponseEntity updateUser(@RequestBody UpdateUserRequestResource updateUserRequest) {
        Long userId = updateUserRequest.getId();
        String fieldName = updateUserRequest.getField();
        Object newFieldValue = updateUserRequest.getNewValue();
        userService.update(userId, fieldName, newFieldValue);
        return ResponseEntity.ok().build();
    }
}
