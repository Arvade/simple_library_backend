package org.bitbucket.arvade.simple_library.repository;

import org.bitbucket.arvade.simple_library.model.Authority;
import org.bitbucket.arvade.simple_library.model.AuthorityName;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AuthorityRepository extends JpaRepository<Authority, Long> {
    Optional<Authority> findByName(AuthorityName name);
}
