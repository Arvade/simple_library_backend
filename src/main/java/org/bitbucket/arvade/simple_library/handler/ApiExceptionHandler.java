package org.bitbucket.arvade.simple_library.handler;

import org.bitbucket.arvade.simple_library.exception.AlreadyExistsException;
import org.bitbucket.arvade.simple_library.exception.NotFoundException;
import org.bitbucket.arvade.simple_library.resource.ApiErrors;
import org.bitbucket.arvade.simple_library.resource.ApiGlobalError;
import org.bitbucket.arvade.simple_library.security.exception.AuthenticationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

@ControllerAdvice
public class ApiExceptionHandler {

    @ExceptionHandler(value = NotFoundException.class)
    public ResponseEntity<Object> handleNotFoundException(NotFoundException e,
                                                          WebRequest request) {
        ApiErrors apiErrors = new ApiErrors();
        apiErrors.addGlobalError(new ApiGlobalError(e.getMessage()));

        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(apiErrors);
    }

    @ExceptionHandler(value = AlreadyExistsException.class)
    public ResponseEntity<Object> handleAlreadyExistsException(AlreadyExistsException e,
                                                               WebRequest request) {
        ApiErrors apiErrors = new ApiErrors();
        apiErrors.addGlobalError(new ApiGlobalError(e.getMessage()));

        return ResponseEntity.status(HttpStatus.CONFLICT).body(apiErrors);
    }

    @ExceptionHandler(value = AuthenticationException.class)
    public ResponseEntity handleAuthenticationException(AuthenticationException e) {
        ApiErrors apiErrors = new ApiErrors();
        apiErrors.addGlobalError(new ApiGlobalError(e.getMessage()));

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(apiErrors);
    }
}
