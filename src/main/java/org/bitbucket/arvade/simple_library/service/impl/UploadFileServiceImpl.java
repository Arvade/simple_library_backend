package org.bitbucket.arvade.simple_library.service.impl;

import org.bitbucket.arvade.simple_library.model.UploadFile;
import org.bitbucket.arvade.simple_library.repository.UploadFileRepository;
import org.bitbucket.arvade.simple_library.resource.FileResource;
import org.bitbucket.arvade.simple_library.service.UploadFileService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
public class UploadFileServiceImpl implements UploadFileService {

    private UploadFileRepository uploadFileRepository;
    private ModelMapper modelMapper;

    @Autowired
    public UploadFileServiceImpl(UploadFileRepository uploadFileRepository, ModelMapper modelMapper) {
        this.uploadFileRepository = uploadFileRepository;
        this.modelMapper = modelMapper;
    }

    @Override
    @Transactional
    public Long save(String fileName, byte[] data) {
        UploadFile uploadFile = new UploadFile();
        uploadFile.setFileName(fileName);
        uploadFile.setData(data);

        UploadFile saved = uploadFileRepository.save(uploadFile);
        return saved.getId();
    }

    @Override
    public Optional<FileResource> findById(Long id) {
        return uploadFileRepository.findById(id)
                .map(file -> modelMapper.map(file, FileResource.class));
    }
}
