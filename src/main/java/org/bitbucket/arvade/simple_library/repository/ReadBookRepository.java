package org.bitbucket.arvade.simple_library.repository;

import org.bitbucket.arvade.simple_library.model.ReadBook;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface ReadBookRepository extends JpaRepository<ReadBook, Long> {

    boolean existsByBook_IdAndUser_username(Long bookId, String username);

    Optional<ReadBook> findByBook_IdAndUser_Username(Long bookId, String username);

    List<ReadBook> findByUser_Username(String username);
}

