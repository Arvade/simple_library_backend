package org.bitbucket.arvade.simple_library.service;

import org.bitbucket.arvade.simple_library.model.Book;
import org.bitbucket.arvade.simple_library.model.User;
import org.bitbucket.arvade.simple_library.resource.book.BookHomePageResource;

import java.util.List;

public interface LikeBookService {

    boolean isBookLikedByUser(Long bookId, String username);

    void likeBookForUser(Long bookId, String username);

    void likeBookForUser(Book book, User user);

    void unlikeBookForUser(Long bookId, String username);

    List<BookHomePageResource> getAllLikedBooksForUser(String username);
}
