package org.bitbucket.arvade.simple_library.service;

import org.bitbucket.arvade.simple_library.model.Book;
import org.bitbucket.arvade.simple_library.model.User;
import org.bitbucket.arvade.simple_library.resource.book.BookHomePageResource;

import java.util.List;

public interface ReadBookService {

    void setBookAsReadForUser(Book book, User user);

    void setBookAsReadForUser(Long bookId, String username);

    void setBookAsUnreadForUser(Long bookId, String username);

    boolean isBookSetAsReadForUser(Long bookId, String username);

    List<BookHomePageResource> getReadBooksForUser(String username);
}
