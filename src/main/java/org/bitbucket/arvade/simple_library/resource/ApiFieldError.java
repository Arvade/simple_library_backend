package org.bitbucket.arvade.simple_library.resource;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ApiFieldError {

    private String field;
    private String code;
    private Object rejectedValue;
}
