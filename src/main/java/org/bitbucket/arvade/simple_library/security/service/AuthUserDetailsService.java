package org.bitbucket.arvade.simple_library.security.service;

import org.bitbucket.arvade.simple_library.model.User;
import org.bitbucket.arvade.simple_library.repository.UserRepository;
import org.bitbucket.arvade.simple_library.security.factory.AuthUserFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AuthUserDetailsService implements UserDetailsService {

    private UserRepository userRepository;
    private AuthUserFactory authUserFactory;

    @Autowired
    public AuthUserDetailsService(UserRepository userRepository,
                                  AuthUserFactory authUserFactory) {
        this.userRepository = userRepository;
        this.authUserFactory = authUserFactory;
    }

    @Override
    public UserDetails loadUserByUsername(String username) {
        Optional<User> userOptional = userRepository.findByUsername(username);
        return authUserFactory.create(userOptional.orElseThrow(() ->
                new UsernameNotFoundException("No user found for username '" + username + "'")));
    }
}