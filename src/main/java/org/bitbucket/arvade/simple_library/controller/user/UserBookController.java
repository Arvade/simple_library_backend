package org.bitbucket.arvade.simple_library.controller.user;

import org.bitbucket.arvade.simple_library.exception.NotFoundException;
import org.bitbucket.arvade.simple_library.resource.book.*;
import org.bitbucket.arvade.simple_library.service.BookService;
import org.bitbucket.arvade.simple_library.service.LikeBookService;
import org.bitbucket.arvade.simple_library.service.ReadBookService;
import org.bitbucket.arvade.simple_library.service.ToReadBookService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@CrossOrigin(value = "*")
@RestController
@PreAuthorize(value = "hasAnyRole('USER', 'ADMIN')")
public class UserBookController {

    private ToReadBookService toReadBookService;
    private ReadBookService readbookService;
    private LikeBookService likeBookService;
    private BookService bookService;
    private ModelMapper modelMapper;

    @Autowired
    public UserBookController(ToReadBookService toReadBookService,
                              ReadBookService readbookService,
                              LikeBookService likeBookService,
                              BookService bookService,
                              ModelMapper modelMapper) {
        this.toReadBookService = toReadBookService;
        this.readbookService = readbookService;
        this.likeBookService = likeBookService;
        this.bookService = bookService;
        this.modelMapper = modelMapper;
    }

    @GetMapping(value = "/app/books")
    public ResponseEntity<List<LoggedUserBookHomePageResource>> getBooks() {
        String username = getLoggedInUsername();
        List<LoggedUserBookHomePageResource> resultValue = new ArrayList<>();
        List<BookWithCoverResource> bookList = bookService.findAll();

        List<BookHomePageResource> homePageResources = readbookService.getReadBooksForUser(username);

        for (BookWithCoverResource book : bookList) {
            LoggedUserBookHomePageResource loggedUserBookHomePageResource = modelMapper.map(book, LoggedUserBookHomePageResource.class);
            if (containsAndRemoveIfPresent(homePageResources, book)) {
                loggedUserBookHomePageResource.setRead(true);
            }
            resultValue.add(loggedUserBookHomePageResource);
        }
        return ResponseEntity.ok(resultValue);
    }

    @GetMapping(value = "/app/books/{id}")
    public ResponseEntity<LoggedUserBookHomePageResource> getBook(@PathVariable Long id) {
        String username = getLoggedInUsername();

        BookWithCoverResource book = bookService.findBookWithCoverById(id)
                .orElseThrow(() -> new NotFoundException("No book found for id " + id));

        LoggedUserBookHomePageResource resultValue = modelMapper.map(book, LoggedUserBookHomePageResource.class);
        resultValue.setRead(readbookService.isBookSetAsReadForUser(id, username));
        resultValue.setTodo(toReadBookService.isBookSetToReadForUser(id, username));
        resultValue.setLike(likeBookService.isBookLikedByUser(id, username));

        return ResponseEntity.ok(resultValue);
    }

    private boolean containsAndRemoveIfPresent(List<BookHomePageResource> list, BookWithCoverResource book) {
        for (BookHomePageResource homePageResource : list) {
            if (homePageResource.getBookId().equals(book.getId())) {
                list.remove(homePageResource);
                return true;
            }
        }
        return false;
    }

    private String getLoggedInUsername() {
        return SecurityContextHolder.getContext().getAuthentication().getName();
    }
}