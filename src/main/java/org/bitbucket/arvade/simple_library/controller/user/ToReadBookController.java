package org.bitbucket.arvade.simple_library.controller.user;

import org.bitbucket.arvade.simple_library.resource.book.BookHomePageResource;
import org.bitbucket.arvade.simple_library.service.ToReadBookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(value = "*")
@RestController
@PreAuthorize("hasAnyRole('USER', 'ADMIN')")
public class ToReadBookController {

    private ToReadBookService toReadBookService;

    @Autowired
    public ToReadBookController(ToReadBookService toReadBookService) {
        this.toReadBookService = toReadBookService;
    }

    @GetMapping(value = "/app/books/toRead")
    public ResponseEntity getToReadBooksForCurrentUser() {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        List<BookHomePageResource> homePageResources = toReadBookService.getToReadBooksForUser(username);
        return ResponseEntity.ok(homePageResources);
    }

    @GetMapping(value = "/app/books/markAsToRead/{id}")
    public ResponseEntity setBookAsToRead(@PathVariable Long id) {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        toReadBookService.setBookAsToReadForUser(id, username);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping(value = "/app/books/markAsToRead/{id}")
    public void setBookAsNotToRead(@PathVariable Long id) {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        toReadBookService.setBookAsNotToReadForUser(id, username);
    }
}
