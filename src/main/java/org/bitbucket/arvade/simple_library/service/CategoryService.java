package org.bitbucket.arvade.simple_library.service;

import org.bitbucket.arvade.simple_library.resource.CategoryHomePageResource;
import org.bitbucket.arvade.simple_library.resource.CategoryResource;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Optional;

public interface CategoryService {

    void create(String name);

    void update(Long id, String newName);

    void delete(Long id);

    List<CategoryResource> findAll();

    List<CategoryHomePageResource> findAllForHomePage();

    Optional<CategoryResource> findByName(String name);

    Page<CategoryResource> findCategoriesPaginated(Integer page, Integer size);
}
