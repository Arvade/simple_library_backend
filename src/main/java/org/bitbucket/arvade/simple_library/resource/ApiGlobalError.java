package org.bitbucket.arvade.simple_library.resource;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ApiGlobalError {

    private String code;
}
