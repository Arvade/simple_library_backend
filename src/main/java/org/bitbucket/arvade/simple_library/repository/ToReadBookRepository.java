package org.bitbucket.arvade.simple_library.repository;

import org.bitbucket.arvade.simple_library.model.ToReadBook;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ToReadBookRepository extends JpaRepository<ToReadBook, Long> {

    void deleteByBook_IdAndUser_Username(Long bookId, String username);

    boolean existsByBook_IdAndUser_Username(Long bookId, String username);

    List<ToReadBook> findAllByUser_Username(String username);
}
