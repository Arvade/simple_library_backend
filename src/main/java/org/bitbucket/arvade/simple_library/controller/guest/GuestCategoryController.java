package org.bitbucket.arvade.simple_library.controller.guest;

import org.bitbucket.arvade.simple_library.resource.CategoryHomePageResource;
import org.bitbucket.arvade.simple_library.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@CrossOrigin("*")
public class GuestCategoryController {

    private CategoryService categoryService;

    @Autowired
    public GuestCategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @GetMapping(value = "/categories")
    public ResponseEntity<List<CategoryHomePageResource>> getCategoriesWithBooks() {
        List<CategoryHomePageResource> allForHomePage = categoryService.findAllForHomePage().stream()
                .filter(categoryHomePageResource -> !categoryHomePageResource.getBookList().isEmpty())
                .collect(Collectors.toList());
        return ResponseEntity.ok(allForHomePage);
    }
}
