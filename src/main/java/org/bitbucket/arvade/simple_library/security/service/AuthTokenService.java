package org.bitbucket.arvade.simple_library.security.service;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Clock;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.impl.DefaultClock;
import org.bitbucket.arvade.simple_library.security.model.AuthUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class AuthTokenService {

    private AuthTokenExtractor authTokenExtractor;
    private AuthTokenUtil authTokenUtil;
    private Clock clock = DefaultClock.INSTANCE;

    @Value(value = "${jwt.secret}")
    private String secret;

    @Autowired
    public AuthTokenService(AuthTokenExtractor authTokenExtractor, AuthTokenUtil authTokenUtil) {
        this.authTokenExtractor = authTokenExtractor;
        this.authTokenUtil = authTokenUtil;
    }

    public String refresh(String token) {
        Date creationDate = clock.now();
        Date expirationDate = authTokenUtil.calculateExpirationDate(creationDate);

        Claims claims = authTokenExtractor.getAllClaimsFrom(token);
        claims.setIssuedAt(creationDate);
        claims.setExpiration(expirationDate);

        return Jwts.builder()
                .setClaims(claims)
                .signWith(SignatureAlgorithm.HS512, secret)
                .compact();
    }

    public Boolean validate(String token, UserDetails userDetails) {
        AuthUser user = (AuthUser) userDetails;
        final String username = authTokenExtractor.getUsernameFrom(token);
        final Date creationDate = authTokenExtractor.getIssuedAtDateFrom(token);

        return (username.equals(user.getUsername())
                && !isExpired(token)
                && !isCreatedBeforeLastPasswordReset(creationDate, user.getLastPasswordResetDate()));
    }

    public Boolean canBeRefreshed(String token, Date lastPasswordReset) {
        Date creationDate = authTokenExtractor.getIssuedAtDateFrom(token);
        return (!isCreatedBeforeLastPasswordReset(creationDate, lastPasswordReset)
                && !isExpired(token));
    }

    private Boolean isExpired(String token) {
        return authTokenExtractor.getExpirationDateFrom(token).before(clock.now());
    }

    private Boolean isCreatedBeforeLastPasswordReset(Date creationDate, Date lastPasswordReset) {
        return (lastPasswordReset != null && creationDate.before(lastPasswordReset));
    }
}
