package org.bitbucket.arvade.simple_library.security.resource;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class AuthResponseResource {

    private final String token;
}
