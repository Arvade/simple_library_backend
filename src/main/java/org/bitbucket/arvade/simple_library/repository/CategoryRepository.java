package org.bitbucket.arvade.simple_library.repository;

import org.bitbucket.arvade.simple_library.model.Category;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;
import java.util.Optional;

public interface CategoryRepository extends JpaRepository<Category, Long> {

    Optional<Category> findByName(String name);

    @Transactional
    void deleteById(Long id);
}
