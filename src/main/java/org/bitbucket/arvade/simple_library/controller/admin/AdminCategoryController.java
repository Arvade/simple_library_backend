package org.bitbucket.arvade.simple_library.controller.admin;

import org.bitbucket.arvade.simple_library.resource.*;
import org.bitbucket.arvade.simple_library.resource.request.DeleteRequestResource;
import org.bitbucket.arvade.simple_library.resource.request.NameRequestResource;
import org.bitbucket.arvade.simple_library.resource.request.UpdateRequestCategoryResource;
import org.bitbucket.arvade.simple_library.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@PreAuthorize("hasRole('ADMIN')")
@CrossOrigin("*")
public class AdminCategoryController {

    private CategoryService categoryService;

    @Autowired
    public AdminCategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @GetMapping(value = "/app/admin/categories/{page}/{size}")
    public ResponseEntity getCategories(@PathVariable Integer page,
                                        @PathVariable Integer size) {
        Page<CategoryResource> resultPage = categoryService.findCategoriesPaginated(page, size);
        return ResponseEntity.ok(resultPage);
    }

    @PostMapping(value = "/app/admin/category/create")
    public ResponseEntity addCategory(@RequestBody @Valid NameRequestResource nameRequest) {
        String name = nameRequest.getName();
        categoryService.create(name);
        return ResponseEntity.ok().build();
    }

    @PostMapping(value = "/app/admin/category/update")
    public ResponseEntity editCategory(@RequestBody @Valid UpdateRequestCategoryResource updateCategory) {
        Long id = updateCategory.getId();
        String name = updateCategory.getName();
        categoryService.update(id, name);
        return ResponseEntity.ok().build();
    }

    @PostMapping(value = "/app/admin/category/delete")
    public ResponseEntity deleteCategory(@RequestBody @Valid DeleteRequestResource deleteRequest) {
        Long id = deleteRequest.getId();
        categoryService.delete(id);
        return ResponseEntity.ok().build();
    }

    @PostMapping(value = "/app/admin/category/exists")
    public ResponseEntity<DoesExistsResource> categoryExists(@RequestBody @Valid NameRequestResource nameRequest) {
        DoesExistsResource doesExists = new DoesExistsResource();
        boolean exists = categoryService.findByName(nameRequest.getName()).isPresent();
        doesExists.setExists(exists);
        return ResponseEntity.ok(doesExists);
    }
}
