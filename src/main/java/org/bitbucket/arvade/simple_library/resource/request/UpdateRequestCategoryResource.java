package org.bitbucket.arvade.simple_library.resource.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UpdateRequestCategoryResource {

    @NotNull(message = "Id cannot be null")
    private Long id;

    @NotEmpty(message = "You need to provide new name")
    private String name;
}
