package org.bitbucket.arvade.simple_library.service.impl;

import org.bitbucket.arvade.simple_library.exception.AlreadyExistsException;
import org.bitbucket.arvade.simple_library.exception.NotFoundException;
import org.bitbucket.arvade.simple_library.model.Category;
import org.bitbucket.arvade.simple_library.repository.CategoryRepository;
import org.bitbucket.arvade.simple_library.resource.CategoryHomePageResource;
import org.bitbucket.arvade.simple_library.resource.CategoryResource;
import org.bitbucket.arvade.simple_library.resource.book.BookHomePageResource;
import org.bitbucket.arvade.simple_library.resource.book.CompleteBookResource;
import org.bitbucket.arvade.simple_library.service.BookService;
import org.bitbucket.arvade.simple_library.service.CategoryService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CategoryServiceImpl implements CategoryService {

    private CategoryRepository categoryRepository;
    private BookService bookService;
    private ModelMapper modelMapper;

    @Autowired
    public CategoryServiceImpl(CategoryRepository categoryRepository,
                               BookService bookService,
                               ModelMapper modelMapper) {
        this.categoryRepository = categoryRepository;
        this.bookService = bookService;
        this.modelMapper = modelMapper;
    }

    @Override
    public void create(String name) {
        if (findByName(name).isPresent()) {
            throw new AlreadyExistsException("Category already exists for name " + name);
        }

        Category category = new Category();
        category.setName(name.toUpperCase());
        categoryRepository.save(category);
    }

    @Override
    @Transactional
    public void update(Long id, String newName) {
        Category category = categoryRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("No category found for id " + id));
        category.setName(newName.toUpperCase());
        categoryRepository.save(category);
    }

    @Override
    public void delete(Long id) {
        categoryRepository.deleteById(id);
    }

    @Override
    public List<CategoryResource> findAll() {
        return categoryRepository.findAll().stream()
                .map(category -> modelMapper.map(category, CategoryResource.class))
                .collect(Collectors.toList());
    }

    @Override
    public List<CategoryHomePageResource> findAllForHomePage() {
        List<CategoryHomePageResource> resultValue = new ArrayList<>();
        List<Category> categories = categoryRepository.findAll();

        for (Category category : categories) {
            CategoryHomePageResource categoryHomePage = new CategoryHomePageResource();
            categoryHomePage.setId(category.getId());
            categoryHomePage.setName(category.getName());

            categoryHomePage.setBookList(category.getBooks().stream()
                    .map(book -> {
                        BookHomePageResource resource = new BookHomePageResource();
                        CompleteBookResource completeBook = bookService.findCompleteBookById(book.getId())
                                .orElseThrow(() -> new NotFoundException("No book found for id " + book.getId()));
                        resource.setBookId(completeBook.getId());
                        resource.setBookTitle(completeBook.getTitle());
                        resource.setBookCoverImageData(completeBook.getCoverImageData());

                        return resource;
                    })
                    .collect(Collectors.toList()));
            resultValue.add(categoryHomePage);
        }

        return resultValue;
    }

    @Override
    public Optional<CategoryResource> findByName(String name) {
        return categoryRepository.findByName(name)
                .map(category -> modelMapper.map(category, CategoryResource.class));
    }

    @Override
    public Page<CategoryResource> findCategoriesPaginated(Integer page, Integer size) {
        return categoryRepository.findAll(PageRequest.of(page, size))
                .map(category -> modelMapper.map(category, CategoryResource.class));
    }
}
