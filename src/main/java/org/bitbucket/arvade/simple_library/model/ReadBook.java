package org.bitbucket.arvade.simple_library.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ReadBook {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @OneToOne(cascade = CascadeType.MERGE)
    private Book book;

    @OneToOne(cascade = CascadeType.MERGE)
    private User user;

    public ReadBook(Book book, User user) {
        this.book = book;
        this.user = user;
    }
}
