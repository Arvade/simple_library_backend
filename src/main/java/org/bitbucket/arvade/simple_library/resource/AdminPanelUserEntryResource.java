package org.bitbucket.arvade.simple_library.resource;

import lombok.Data;
import org.bitbucket.arvade.simple_library.model.Authority;

import java.util.List;

@Data
public class AdminPanelUserEntryResource {

    private Long id;

    private String username;

    private String firstname;

    private String lastname;

    private String email;

    private Boolean enabled;

    private List<Authority> authorities;
}
