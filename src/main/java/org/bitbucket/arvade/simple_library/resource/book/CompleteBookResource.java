package org.bitbucket.arvade.simple_library.resource.book;

import lombok.Data;
import org.bitbucket.arvade.simple_library.model.Category;

import java.util.Date;
import java.util.Set;

@Data
public class CompleteBookResource {

    private Long id;

    private String title;

    private String description;

    private byte[] coverImageData;

    private byte[] contentData;

    private Date releaseDate;

    private Set<Category> categories;
}
