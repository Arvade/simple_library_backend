package org.bitbucket.arvade.simple_library.service;

import io.jsonwebtoken.Clock;
import org.assertj.core.util.DateUtil;
import org.bitbucket.arvade.simple_library.security.factory.AuthTokenFactory;
import org.bitbucket.arvade.simple_library.security.model.UserDetailsDummy;
import org.bitbucket.arvade.simple_library.security.service.AuthTokenExtractor;
import org.bitbucket.arvade.simple_library.security.service.AuthTokenUtil;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.Date;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AuthTokenExtractorTest {

    private static final String USERNAME = "JohnSnow";
    private static final String SECRET = "secret";
    private static final Long EXPIRATION_ONE_HOUR = 3600L;

    @Mock
    private Clock clock;
    private AuthTokenUtil authTokenUtil;
    private AuthTokenFactory authTokenFactory;

    private AuthTokenExtractor extractor;

    @Before
    public void setup() {
        this.authTokenUtil = new AuthTokenUtil();
        this.authTokenFactory = new AuthTokenFactory(authTokenUtil);
        this.extractor = new AuthTokenExtractor();
        ReflectionTestUtils.setField(extractor, "secret", SECRET);
        ReflectionTestUtils.setField(extractor, "expiration", EXPIRATION_ONE_HOUR);
        ReflectionTestUtils.setField(authTokenUtil, "expiration", EXPIRATION_ONE_HOUR);
        ReflectionTestUtils.setField(authTokenFactory, "clock", clock);
        ReflectionTestUtils.setField(authTokenFactory, "secret", SECRET);
    }

    @Test
    public void shouldReturnUsernameFromToken() {
        //Given
        when(clock.now()).thenReturn(DateUtil.now());
        String token = createToken();

        //When
        String result = extractor.getUsernameFrom(token);

        //Then
        assertThat(result).isEqualTo(USERNAME);
    }

    @Test
    public void shouldReturnIssuedDateFromToken() {
        //Given
        Date now = DateUtil.now();
        when(clock.now()).thenReturn(now);

        String token = createToken();

        //When
        Date result = extractor.getIssuedAtDateFrom(token);

        //Then
        assertThat(result).isInSameMinuteWindowAs(now);
    }

    @Test
    public void shouldReturnExpirationDateFromToken() {
        //Given
        Date now = DateUtil.now();
        Date expectedExpirationDate = new Date(now.getTime() + EXPIRATION_ONE_HOUR);
        when(clock.now()).thenReturn(now);

        String token = createToken();

        //When
        Date result = extractor.getExpirationDateFrom(token);

        //Then
        assertThat(result).isInSameHourWindowAs(expectedExpirationDate);
    }

    private String createToken() {
        return authTokenFactory.create(new UserDetailsDummy(USERNAME));
    }
}
