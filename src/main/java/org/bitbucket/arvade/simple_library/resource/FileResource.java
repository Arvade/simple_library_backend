package org.bitbucket.arvade.simple_library.resource;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FileResource {

    private Long id;

    private String fileName;

    private byte[] data;
}
