package org.bitbucket.arvade.simple_library.service.impl;

import org.bitbucket.arvade.simple_library.model.Book;
import org.bitbucket.arvade.simple_library.model.Category;
import org.bitbucket.arvade.simple_library.model.UploadFile;
import org.bitbucket.arvade.simple_library.repository.BookRepository;
import org.bitbucket.arvade.simple_library.resource.AddBookResource;
import org.bitbucket.arvade.simple_library.resource.book.BookWithCoverResource;
import org.bitbucket.arvade.simple_library.resource.book.CompleteBookResource;
import org.bitbucket.arvade.simple_library.service.BookService;
import org.bitbucket.arvade.simple_library.service.UploadFileService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class BookServiceImpl implements BookService {

    private BookRepository bookRepository;
    private UploadFileService uploadFileService;
    private ModelMapper modelMapper;

    @Autowired
    public BookServiceImpl(BookRepository bookRepository,
                           UploadFileService uploadFileService,
                           ModelMapper modelMapper) {
        this.bookRepository = bookRepository;
        this.uploadFileService = uploadFileService;
        this.modelMapper = modelMapper;
    }

    @Override
    public List<CompleteBookResource> findAllBooksWithContent() {
        return bookRepository.findAll().stream()
                .map(book -> modelMapper.map(book, CompleteBookResource.class))
                .collect(Collectors.toList());
    }

    @Override
    public List<BookWithCoverResource> findAll() {
        return bookRepository.findAll().stream()
                .map(book -> modelMapper.map(book, BookWithCoverResource.class))
                .collect(Collectors.toList());
    }

    @Override
    public Optional<CompleteBookResource> findCompleteBookById(Long id) {
        return bookRepository.findById(id)
                .map(book -> modelMapper.map(book, CompleteBookResource.class));
    }

    @Override
    public Optional<BookWithCoverResource> findBookWithCoverById(Long id) {
        return bookRepository.findById(id)
                .map(book -> modelMapper.map(book, BookWithCoverResource.class));
    }

    @Override
    public void addBook(AddBookResource addBookResource) {
        Book book = new Book();

        Long coverImageId = addBookResource.getCoverImageId();
        Long contentId = addBookResource.getContentId();
        UploadFile coverImage = null;
        UploadFile bookContent = null;

        if (coverImageId != null) {
            coverImage = uploadFileService.findById(coverImageId)
                    .map(fileResource -> modelMapper.map(fileResource, UploadFile.class))
                    .orElse(null);
        }
        if (contentId != null) {
            bookContent = uploadFileService.findById(contentId)
                    .map(fileResource -> modelMapper.map(fileResource, UploadFile.class))
                    .orElse(null);
        }


        book.setCoverImage(coverImage);
        book.setContent(bookContent);
        book.setTitle(addBookResource.getTitle());
        book.setDescription(addBookResource.getDescription());
        book.setCategories(Arrays.stream(addBookResource.getCategories())
                .map(Category::new)
                .collect(Collectors.toSet()));
        bookRepository.save(book);
    }
}
