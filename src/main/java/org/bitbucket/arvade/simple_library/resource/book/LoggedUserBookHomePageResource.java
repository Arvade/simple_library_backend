package org.bitbucket.arvade.simple_library.resource.book;

import lombok.Data;
import org.bitbucket.arvade.simple_library.model.Category;

import java.util.Set;

@Data
public class LoggedUserBookHomePageResource {

    private Long id;

    private String title;

    private String description;

    private byte[] coverImageData;

    private Long contentId;

    private Set<Category> categories;

    private boolean isRead = false;

    private boolean isTodo = false;

    private boolean isLike = false;
}
