package org.bitbucket.arvade.simple_library.security.service;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.function.Function;

@Component
public class AuthTokenExtractor {

    @Value(value = "${jwt.secret}")
    private String secret;

    @Value(value = "${jwt.expiration}")
    private Long expiration;

    public String getUsernameFrom(String token) {
        return getClaimFrom(token, Claims::getSubject);
    }

    public Date getIssuedAtDateFrom(String token) {
        return getClaimFrom(token, Claims::getIssuedAt);
    }

    public Date getExpirationDateFrom(String token) {
        return getClaimFrom(token, Claims::getExpiration);
    }

    public Claims getAllClaimsFrom(String token) {
        return Jwts.parser()
                .setSigningKey(secret)
                .parseClaimsJws(token)
                .getBody();
    }

    private <T> T getClaimFrom(String token, Function<Claims, T> claimsResolver) {
        Claims claims = getAllClaimsFrom(token);
        return claimsResolver.apply(claims);
    }
}
