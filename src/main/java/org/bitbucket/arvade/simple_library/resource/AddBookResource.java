package org.bitbucket.arvade.simple_library.resource;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AddBookResource {

    @NotEmpty
    private String title;

    @NotEmpty
    private String description;

    private Long coverImageId;

    private Long contentId;

    private Long[] categories;
}
