package org.bitbucket.arvade.simple_library.controller.user;

import org.bitbucket.arvade.simple_library.resource.book.BookHomePageResource;
import org.bitbucket.arvade.simple_library.service.LikeBookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(value = "*")
@RestController
@PreAuthorize("hasAnyRole('USER','ADMIN')")
public class LikedBooksController {

    private LikeBookService likeBookService;

    @Autowired
    public LikedBooksController(LikeBookService likeBookService) {
        this.likeBookService = likeBookService;
    }

    @GetMapping(value = "/app/books/like")
    public ResponseEntity getLikedBooksForCurrentUser() {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        List<BookHomePageResource> likedBooks = likeBookService.getAllLikedBooksForUser(username);
        return ResponseEntity.ok(likedBooks);
    }

    @GetMapping(value = "/app/books/like/{id}")
    public void likeBook(@PathVariable Long id) {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        likeBookService.likeBookForUser(id, username);
    }

    @DeleteMapping(value = "/app/books/like/{id}")
    public void unlikeBook(@PathVariable Long id) {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        likeBookService.unlikeBookForUser(id, username);
    }
}