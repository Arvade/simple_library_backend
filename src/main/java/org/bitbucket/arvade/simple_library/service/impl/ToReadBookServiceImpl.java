package org.bitbucket.arvade.simple_library.service.impl;

import org.bitbucket.arvade.simple_library.exception.NotFoundException;
import org.bitbucket.arvade.simple_library.model.Book;
import org.bitbucket.arvade.simple_library.model.ToReadBook;
import org.bitbucket.arvade.simple_library.model.User;
import org.bitbucket.arvade.simple_library.repository.ToReadBookRepository;
import org.bitbucket.arvade.simple_library.resource.book.BookHomePageResource;
import org.bitbucket.arvade.simple_library.service.BookService;
import org.bitbucket.arvade.simple_library.service.ToReadBookService;
import org.bitbucket.arvade.simple_library.service.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ToReadBookServiceImpl implements ToReadBookService {

    private BookService bookService;
    private UserService userService;
    private ToReadBookRepository toReadBookRepository;
    private ModelMapper modelMapper;

    @Autowired
    public ToReadBookServiceImpl(BookService bookService,
                                 UserService userService,
                                 ToReadBookRepository toReadBookRepository,
                                 ModelMapper modelMapper) {
        this.bookService = bookService;
        this.userService = userService;
        this.toReadBookRepository = toReadBookRepository;
        this.modelMapper = modelMapper;
    }

    @Override
    public void setBookAsToReadForUser(Long bookId, String username) {
        Book book = bookService.findCompleteBookById(bookId)
                .map(bookResource -> modelMapper.map(bookResource, Book.class))
                .orElseThrow(() -> new NotFoundException("No book found for id " + bookId));

        User user = userService.findByUsername(username)
                .map(userInformationResource -> modelMapper.map(userInformationResource, User.class))
                .orElseThrow(() -> new NotFoundException("No user found for username " + username));

        setBookAsToReadForUser(book, user);
    }

    @Override
    public void setBookAsToReadForUser(Book book, User user) {
        if (isBookSetToReadForUser(book.getId(), user.getUsername())) return;
        ToReadBook toReadBook = new ToReadBook(book, user);
        toReadBookRepository.save(toReadBook);
    }

    @Override
    @Transactional
    public void setBookAsNotToReadForUser(Long bookId, String username) {
        toReadBookRepository.deleteByBook_IdAndUser_Username(bookId, username);
    }

    @Override
    public boolean isBookSetToReadForUser(Long bookId, String username) {
        return toReadBookRepository.existsByBook_IdAndUser_Username(bookId, username);
    }

    @Override
    public List<BookHomePageResource> getToReadBooksForUser(String username) {
        return toReadBookRepository.findAllByUser_Username(username).stream()
                .map(toReadBook -> modelMapper.map(toReadBook, BookHomePageResource.class))
                .collect(Collectors.toList());
    }
}