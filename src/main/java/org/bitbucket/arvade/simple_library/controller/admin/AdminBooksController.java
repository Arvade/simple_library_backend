package org.bitbucket.arvade.simple_library.controller.admin;

import org.bitbucket.arvade.simple_library.resource.AddBookResource;
import org.bitbucket.arvade.simple_library.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@CrossOrigin("*")
@RestController
@PreAuthorize("hasRole('ADMIN')")
public class AdminBooksController {

    private BookService bookService;

    @Autowired
    public AdminBooksController(BookService bookService) {
        this.bookService = bookService;
    }

    @PostMapping(value = "/app/admin/books/add")
    public ResponseEntity addBook(@RequestBody @Valid AddBookResource addBookResource) {
        bookService.addBook(addBookResource);
        return ResponseEntity.ok().build();
    }
}
