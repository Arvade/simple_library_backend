package org.bitbucket.arvade.simple_library.model;

public enum AuthorityName {
    ROLE_USER, ROLE_ADMIN, ROLE_OWNER
}
