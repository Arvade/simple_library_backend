package org.bitbucket.arvade.simple_library.resource;

import lombok.Data;

@Data
public class CategoryResource {

    private Long id;

    private String name;
}
