package org.bitbucket.arvade.simple_library.security.resource;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AuthRequestResource {

    private String username;
    private String password;
}
