package org.bitbucket.arvade.simple_library.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "book")
@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
public class Book {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String title;

    @Column
    private String description;

    @OneToOne(cascade = CascadeType.MERGE)
    private UploadFile coverImage;

    @OneToOne(cascade = CascadeType.MERGE)
    private UploadFile content;

    @Temporal(TemporalType.TIMESTAMP)
    private Date releaseDate;

    @ManyToMany
    @JoinTable(name = "book_category",
            joinColumns = {@JoinColumn(name = "book_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "category_id", referencedColumnName = "id")})
    private Set<Category> categories;
}
