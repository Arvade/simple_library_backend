package org.bitbucket.arvade.simple_library.resource;

import lombok.Data;
import org.bitbucket.arvade.simple_library.model.Authority;

import java.util.Date;
import java.util.List;

@Data
public class UserInformationResource {

    private Long id;
    private String username;
    private String firstname;
    private String lastname;
    private String email;
    private Date lastPasswordResetDate;
    private List<Authority> authorities;
}
