package org.bitbucket.arvade.simple_library.security.factory;

import io.jsonwebtoken.Clock;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.impl.DefaultClock;
import org.bitbucket.arvade.simple_library.security.service.AuthTokenUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;


@Component
public class AuthTokenFactory {

    private Clock clock = DefaultClock.INSTANCE;

    private AuthTokenUtil authTokenUtil;

    @Value(value = "${jwt.secret}")
    private String secret;

    @Autowired
    public AuthTokenFactory(AuthTokenUtil authTokenUtil) {
        this.authTokenUtil = authTokenUtil;
    }

    public String create(UserDetails userDetails) {
        Map<String, Object> claims = new HashMap<>();
        claims.put("role", userDetails.getAuthorities());
        return doCreate(claims, userDetails.getUsername());
    }

    private String doCreate(Map<String, Object> claims, String subject) {
        Date creationDate = clock.now();
        Date expirationDate = authTokenUtil.calculateExpirationDate(creationDate);

        return Jwts.builder()
                .setClaims(claims)
                .setSubject(subject)
                .setIssuedAt(creationDate)
                .setExpiration(expirationDate)
                .signWith(SignatureAlgorithm.HS512, secret)
                .compact();
    }
}
