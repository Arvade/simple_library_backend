package org.bitbucket.arvade.simple_library.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.bitbucket.arvade.simple_library.model.Authority;
import org.bitbucket.arvade.simple_library.model.AuthorityName;
import org.bitbucket.arvade.simple_library.model.User;
import org.bitbucket.arvade.simple_library.security.factory.AuthTokenFactory;
import org.bitbucket.arvade.simple_library.security.factory.AuthUserFactory;
import org.bitbucket.arvade.simple_library.security.model.AuthUser;
import org.bitbucket.arvade.simple_library.security.resource.AuthRequestResource;
import org.bitbucket.arvade.simple_library.security.service.AuthTokenExtractor;
import org.bitbucket.arvade.simple_library.security.service.AuthTokenService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AuthControllerTest {

    private MockMvc mvc;

    @Autowired
    private WebApplicationContext context;

    @MockBean
    private AuthenticationManager authenticationManager;

    @MockBean
    private UserDetailsService userDetailsService;

    @MockBean
    private AuthTokenExtractor authTokenExtractor;

    @MockBean
    private AuthTokenFactory authTokenFactory;

    @MockBean
    private AuthTokenService authTokenService;

    private AuthUserFactory authUserFactory;

    @Before
    public void setup() {
        this.authUserFactory = new AuthUserFactory();
        mvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();
    }

    @Test
    public void shouldAuthenticateUser() throws Exception {
        //Given
        AuthRequestResource authRequestResource = new AuthRequestResource("username", "password");

        //Then
        mvc.perform(post("/auth")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(authRequestResource)))
                .andExpect(status().is2xxSuccessful());
    }

    @Test
    @WithMockUser
    public void shouldRefreshTokenForUserRole() throws Exception {
        //Given
        User user = createUser("USER");

        AuthUser authUser = authUserFactory.create(user);

        when(authTokenExtractor.getUsernameFrom(any())).thenReturn(user.getUsername());
        when(userDetailsService.loadUserByUsername(user.getUsername())).thenReturn(authUser);
        when(authTokenService.canBeRefreshed(any(), any())).thenReturn(true);

        //Then
        mvc.perform(get("/refresh")
                .header("Authorization", "Bearer 3k1o3ec-b6s9-9ye8-b606-46c9c1bc915a"))
                .andExpect(status().is2xxSuccessful());
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    public void shouldRefreshTokenForAdminRole() throws Exception {
        //Given
        User user = createUser("ADMIN");
        AuthUser authUser = authUserFactory.create(user);

        when(authTokenExtractor.getUsernameFrom(any())).thenReturn(user.getUsername());
        when(userDetailsService.loadUserByUsername(user.getUsername())).thenReturn(authUser);
        when(authTokenService.canBeRefreshed(any(), any())).thenReturn(true);

        //Then
        mvc.perform(get("/refresh")
                .header("Authorization", "Bearer 3k1o3ec-b6s9-9ye8-b606-46c9c1bc915a"))
                .andExpect(status().is2xxSuccessful());
    }

    @Test
    @WithAnonymousUser
    public void shouldReceiveUnauthorizedForAnonymousUser() throws Exception {
        //Then
        mvc.perform(get("/refresh"))
                .andExpect(status().isUnauthorized());
    }

    private User createUser(String role) {
        Authority authority = new Authority();
        authority.setId(1L);
        authority.setName(AuthorityName.valueOf("ROLE_" + role));
        List<Authority> authorityList = Collections.singletonList(authority);

        User user = new User();
        user.setUsername("username");
        user.setAuthorities(authorityList);
        user.setEnabled(true);
        user.setLastPasswordResetDate(new Date());
        return user;
    }
}
