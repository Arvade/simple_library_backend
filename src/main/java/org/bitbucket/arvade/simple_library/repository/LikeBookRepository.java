package org.bitbucket.arvade.simple_library.repository;

import org.bitbucket.arvade.simple_library.model.LikeBook;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface LikeBookRepository extends JpaRepository<LikeBook, Long> {

    void deleteByBook_IdAndUser_Username(Long bookId, String username);

    boolean existsByBook_IdAndUser_Username(Long bookId, String username);

    List<LikeBook> findAllByUser_Username(String username);
}
