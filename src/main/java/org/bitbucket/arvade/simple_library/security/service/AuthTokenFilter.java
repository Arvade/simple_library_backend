package org.bitbucket.arvade.simple_library.security.service;

import io.jsonwebtoken.ExpiredJwtException;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Log4j
@Component
public class AuthTokenFilter extends OncePerRequestFilter {

    private UserDetailsService userDetailsService;
    private AuthTokenService tokenService;
    private AuthTokenExtractor authTokenExtractor;

    @Value("${jwt.header}")
    private String tokenHeader;

    @Autowired
    public AuthTokenFilter(@Qualifier("authUserDetailsService") UserDetailsService userDetailsService,
                           AuthTokenService tokenService,
                           AuthTokenExtractor authTokenExtractor) {
        this.userDetailsService = userDetailsService;
        this.tokenService = tokenService;
        this.authTokenExtractor = authTokenExtractor;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request,
                                    HttpServletResponse response,
                                    FilterChain filterChain) throws ServletException, IOException {
        String requestHeader = request.getHeader(this.tokenHeader);

        String username = null;
        String authToken = null;
        if (isRequestHeaderCorrect(requestHeader)) {
            authToken = extractAuthToken(requestHeader);
            try {
                username = authTokenExtractor.getUsernameFrom(authToken);
            } catch (IllegalArgumentException e) {
                logger.error("Error occurred while extracting username from token", e);
            } catch (ExpiredJwtException e) {
                logger.warn("The token expired and is not valid anymore", e);
            }
        }

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (isUsernameCorrect(username) && checkIfNull(authentication)) {
            UserDetails userDetails = this.userDetailsService.loadUserByUsername(username);

            if (tokenService.validate(authToken, userDetails)) {
                UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(
                        userDetails.getUsername(),
                        null,
                        userDetails.getAuthorities());
                authenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                SecurityContextHolder.getContext().setAuthentication(authenticationToken);
            }
        }

        filterChain.doFilter(request, response);
    }

    private String extractAuthToken(String requestHeader) {
        StringBuilder resultValue = new StringBuilder();
        for (int i = requestHeader.length() - 1; i >= 0; i--) {
            char c = requestHeader.charAt(i);
            if (c == ' ') break;
            resultValue.append(c);
        }
        return resultValue.reverse().toString();
    }

    private boolean isRequestHeaderCorrect(String requestHeader) {
        return (requestHeader != null && requestHeader.startsWith("Bearer "));
    }

    private boolean isUsernameCorrect(String username) {
        return (username != null);
    }

    private boolean checkIfNull(Authentication authentication) {
        return (authentication == null);
    }
}
