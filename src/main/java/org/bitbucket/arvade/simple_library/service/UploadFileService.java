package org.bitbucket.arvade.simple_library.service;

import org.bitbucket.arvade.simple_library.resource.FileResource;

import java.util.Optional;

public interface UploadFileService {

    Long save(String fileName, byte[] data);

    Optional<FileResource> findById(Long id);
}
