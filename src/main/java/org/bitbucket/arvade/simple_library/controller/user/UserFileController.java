package org.bitbucket.arvade.simple_library.controller.user;

import org.bitbucket.arvade.simple_library.exception.NotFoundException;
import org.bitbucket.arvade.simple_library.resource.FileResource;
import org.bitbucket.arvade.simple_library.resource.EntityIdResource;
import org.bitbucket.arvade.simple_library.service.UploadFileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

@RestController
@CrossOrigin("*")
@PreAuthorize("hasRole('USER')")
public class UserFileController {

    private UploadFileService uploadFileService;

    @Autowired
    public UserFileController(UploadFileService uploadFileService) {
        this.uploadFileService = uploadFileService;
    }

    @PostMapping(value = "/app/file")
    public void getFile(@RequestBody EntityIdResource idRequest,
                        HttpServletResponse response) throws IOException {
        Long id = idRequest.getId();
        FileResource fileResource = uploadFileService.findById(id)
                .orElseThrow(() -> new NotFoundException("No file found for id " + id));
        String fileName = fileResource.getFileName();
        byte[] bytes = fileResource.getData();

        response.setContentType("application/octet-stream");
        response.setHeader("Content-Disposition", "inline; filename=\"" + fileName + "\"");
        InputStream inputStream = new BufferedInputStream(new ByteArrayInputStream(bytes));
        FileCopyUtils.copy(inputStream, response.getOutputStream());
    }
}
