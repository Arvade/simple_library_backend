package org.bitbucket.arvade.simple_library.resource.book;

import lombok.Data;

@Data
public class BookHomePageResource {

    private Long bookId;

    private String bookTitle;

    private byte[] bookCoverImageData;
}
