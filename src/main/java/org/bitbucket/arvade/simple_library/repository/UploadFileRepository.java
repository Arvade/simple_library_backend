package org.bitbucket.arvade.simple_library.repository;

import org.bitbucket.arvade.simple_library.model.UploadFile;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UploadFileRepository extends JpaRepository<UploadFile, Long> {
}
