package org.bitbucket.arvade.simple_library.security.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Date;

@AllArgsConstructor
public class AuthUser implements UserDetails {


    private final Long id;

    @Getter
    private final String username;

    @Getter
    private final String firstname;

    @Getter
    private final String lastname;
    private final String password;

    @Getter
    private final String email;

    @Getter
    private final Collection<? extends GrantedAuthority> authorities;

    @Getter
    private final boolean enabled;
    private final Date lastPasswordResetDate;

    @JsonIgnore
    public Long getId() {
        return this.id;
    }

    @JsonIgnore
    @Override
    public String getPassword() {
        return this.password;
    }

    @JsonIgnore
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @JsonIgnore
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @JsonIgnore
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @JsonIgnore
    public Date getLastPasswordResetDate() {
        return this.lastPasswordResetDate;
    }
}
