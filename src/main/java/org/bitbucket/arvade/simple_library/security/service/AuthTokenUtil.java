package org.bitbucket.arvade.simple_library.security.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class AuthTokenUtil {

    @Value(value = "${jwt.expiration}")
    private Long expiration;

    public Date calculateExpirationDate(Date creationDate) {
        return new Date(creationDate.getTime() + expiration * 1000);
    }
}
