package org.bitbucket.arvade.simple_library.service.impl;

import org.bitbucket.arvade.simple_library.exception.NotFoundException;
import org.bitbucket.arvade.simple_library.model.Authority;
import org.bitbucket.arvade.simple_library.model.AuthorityName;
import org.bitbucket.arvade.simple_library.model.User;
import org.bitbucket.arvade.simple_library.repository.AuthorityRepository;
import org.bitbucket.arvade.simple_library.repository.UserRepository;
import org.bitbucket.arvade.simple_library.resource.AdminPanelUserEntryResource;
import org.bitbucket.arvade.simple_library.resource.UserInformationResource;
import org.bitbucket.arvade.simple_library.resource.request.RegistrationRequestResource;
import org.bitbucket.arvade.simple_library.service.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.Field;
import java.util.*;

import static org.bitbucket.arvade.simple_library.model.AuthorityName.ROLE_USER;

@Service
public class UserServiceImpl implements UserService {

    private BCryptPasswordEncoder bCryptPasswordEncoder;
    private AuthorityRepository authorityRepository;
    private UserRepository userRepository;
    private ModelMapper modelMapper;

    @Autowired
    public UserServiceImpl(BCryptPasswordEncoder bCryptPasswordEncoder,
                           AuthorityRepository authorityRepository, UserRepository userRepository,
                           ModelMapper modelMapper) {
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.authorityRepository = authorityRepository;
        this.userRepository = userRepository;
        this.modelMapper = modelMapper;
    }

    @Override
    public void register(RegistrationRequestResource registrationRequestResource) {
        User user = modelMapper.map(registrationRequestResource, User.class);
        String encodedPassword = bCryptPasswordEncoder.encode(user.getPassword());
        user.setPassword(encodedPassword);
        Authority authority = authorityRepository.findByName(ROLE_USER)
                .orElseThrow(() -> new NotFoundException("No role found for name " + ROLE_USER));
        user.setAuthorities(Collections.singletonList(authority));
        user.setLastPasswordResetDate(new Date());
        user.setEnabled(true);
        userRepository.save(user);
    }

    @Override
    @Transactional
    public void update(Long userId, String fieldName, Object value) {
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new NotFoundException("No user found for id " + userId));
        try {
            Field field = user.getClass().getDeclaredField(fieldName);
            field.setAccessible(true);
            if (fieldName.equals("authorities")) {
                Authority authority = authorityRepository
                        .findByName(AuthorityName.valueOf((String) value))
                        .orElseThrow(() -> new NotFoundException("No authority found for name " + value));
                List<Authority> authorities = new ArrayList<>();
                authorities.add(authority);
                user.setAuthorities(authorities);
            } else {
                field.set(user, value);
            }
        } catch (Exception e) {
            throw new RuntimeException("No field found for " + fieldName);
        }
        userRepository.save(user);
    }

    @Override
    public Optional<UserInformationResource> findByUsername(String username) {
        return userRepository.findByUsername(username)
                .map(user -> modelMapper.map(user, UserInformationResource.class));
    }

    @Override
    public Optional<UserInformationResource> findByEmail(String email) {
        return userRepository.findByEmail(email)
                .map(user -> modelMapper.map(user, UserInformationResource.class));
    }

    @Override
    public Optional<UserInformationResource> getUserInformation(String username) {
        return userRepository.findByUsername(username)
                .map(user -> modelMapper.map(user, UserInformationResource.class));
    }

    @Override
    public Page<AdminPanelUserEntryResource> findUserListPaginated(Integer page, Integer size) {
        return userRepository.findAll(PageRequest.of(page, size))
                .map(user -> modelMapper.map(user, AdminPanelUserEntryResource.class));
    }
}