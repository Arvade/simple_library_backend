package org.bitbucket.arvade.simple_library.controller.guest;

import org.bitbucket.arvade.simple_library.resource.DoesExistsResource;
import org.bitbucket.arvade.simple_library.resource.request.NameRequestResource;
import org.bitbucket.arvade.simple_library.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@CrossOrigin("*")
public class GuestUserController {

    private UserService userService;

    @Autowired
    public GuestUserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping(value = "/user/username/exists")
    public ResponseEntity usernameExists(@RequestBody @Valid NameRequestResource nameRequest) {
        boolean exists = userService.findByUsername(nameRequest.getName()).isPresent();
        DoesExistsResource doesExists = new DoesExistsResource(exists);
        return ResponseEntity.ok(doesExists);
    }

    @PostMapping(value = "/user/email/exists")
    public ResponseEntity emailExists(@RequestBody @Valid NameRequestResource nameRequest){
        boolean exists = userService.findByEmail(nameRequest.getName()).isPresent();
        DoesExistsResource doesExists = new DoesExistsResource(exists);
        return ResponseEntity.ok(doesExists);
    }
}
