package org.bitbucket.arvade.simple_library.service;

import org.bitbucket.arvade.simple_library.resource.AddBookResource;
import org.bitbucket.arvade.simple_library.resource.book.BookWithCoverResource;
import org.bitbucket.arvade.simple_library.resource.book.CompleteBookResource;

import java.util.List;
import java.util.Optional;

public interface BookService {

    void addBook(AddBookResource addBookResource);

    Optional<CompleteBookResource> findCompleteBookById(Long id);

    Optional<BookWithCoverResource> findBookWithCoverById(Long id);

    List<CompleteBookResource> findAllBooksWithContent();

    List<BookWithCoverResource> findAll();
}
