package org.bitbucket.arvade.simple_library.resource;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ApiErrors {

    private List<ApiFieldError> fieldErrors = new ArrayList<>();

    private List<ApiGlobalError> globalErrors = new ArrayList<>();

    public void addFieldError(ApiFieldError fieldError) {
        this.fieldErrors.add(fieldError);
    }

    public void addGlobalError(ApiGlobalError globalError) {
        this.globalErrors.add(globalError);
    }

    public void addAllFieldErrors(List<ApiFieldError> fieldErrors) {
        this.fieldErrors.addAll(fieldErrors);
    }

    public void addAllGlobalErrors(List<ApiGlobalError> globalErrors) {
        this.globalErrors.addAll(globalErrors);
    }
}
