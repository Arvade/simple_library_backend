package org.bitbucket.arvade.simple_library.service;

import io.jsonwebtoken.Clock;
import org.assertj.core.util.DateUtil;
import org.bitbucket.arvade.simple_library.security.factory.AuthTokenFactory;
import org.bitbucket.arvade.simple_library.security.model.AuthUser;
import org.bitbucket.arvade.simple_library.security.model.UserDetailsDummy;
import org.bitbucket.arvade.simple_library.security.service.AuthTokenExtractor;
import org.bitbucket.arvade.simple_library.security.service.AuthTokenService;
import org.bitbucket.arvade.simple_library.security.service.AuthTokenUtil;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.Date;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AuthTokenServiceTest {

    private static final String SECRET = "secret";
    private static final Long EXPIRATION_ONE_HOUR = 3600L;
    private static final String USERNAME = "JohnSnow";

    @Mock
    private AuthTokenExtractor authTokenExtractor;

    @Mock
    private AuthTokenUtil authTokenUtil;

    @Mock
    private Clock clock;

    @InjectMocks
    private AuthTokenFactory authTokenFactory;

    @InjectMocks
    private AuthTokenService authTokenService;

    @Before
    public void setup() {
        ReflectionTestUtils.setField(authTokenExtractor, "expiration", EXPIRATION_ONE_HOUR);
        ReflectionTestUtils.setField(authTokenExtractor, "secret", SECRET);
        ReflectionTestUtils.setField(authTokenUtil, "expiration", EXPIRATION_ONE_HOUR);
        ReflectionTestUtils.setField(authTokenFactory, "secret", SECRET);
        ReflectionTestUtils.setField(authTokenFactory, "clock", clock);
        ReflectionTestUtils.setField(authTokenService, "secret", SECRET);
        ReflectionTestUtils.setField(authTokenService, "clock", clock);
    }

    @Test
    public void shouldReturnTrueIfTokenIsValid() {
        //Given
        when(clock.now()).thenReturn(DateUtil.now());
        UserDetails userDetails = mock(AuthUser.class);

        when(userDetails.getUsername()).thenReturn(USERNAME);
        when(authTokenExtractor.getUsernameFrom(anyString())).thenReturn(USERNAME);
        when(authTokenExtractor.getIssuedAtDateFrom(anyString())).thenReturn(DateUtil.now());
        when(authTokenExtractor.getExpirationDateFrom(anyString())).thenReturn(DateUtil.tomorrow());

        when(authTokenUtil.calculateExpirationDate(any())).thenCallRealMethod();
        String token = authTokenFactory.create(userDetails);

        //When
        Boolean result = authTokenService.validate(token, userDetails);

        //Then
        assertThat(result).isTrue();
    }

    @Test
    public void shouldReturnFalseIfTokenIsInvalid() {
        //Given
        when(clock.now()).thenReturn(DateUtil.now());
        UserDetails userDetails = mock(AuthUser.class);

        when(userDetails.getUsername()).thenReturn(USERNAME);
        when(authTokenExtractor.getUsernameFrom(anyString())).thenReturn(USERNAME);
        when(authTokenExtractor.getIssuedAtDateFrom(anyString())).thenReturn(DateUtil.now());
        when(authTokenExtractor.getExpirationDateFrom(anyString())).thenReturn(DateUtil.yesterday());

        when(authTokenUtil.calculateExpirationDate(any())).thenCallRealMethod();
        String token = authTokenFactory.create(userDetails);

        //When
        Boolean result = authTokenService.validate(token, userDetails);

        //Then
        assertThat(result).isFalse();
    }

    @Test
    public void shouldReturnFalseIfUsernamesDoesntMatch() {
        //Given
        when(clock.now()).thenReturn(DateUtil.now());
        UserDetails userDetails = mock(AuthUser.class);
        when(userDetails.getUsername()).thenReturn(USERNAME);
        when(authTokenExtractor.getUsernameFrom(anyString())).thenReturn(USERNAME + "123");
        when(authTokenExtractor.getIssuedAtDateFrom(anyString())).thenReturn(DateUtil.now());
        when(authTokenExtractor.getExpirationDateFrom(anyString())).thenReturn(DateUtil.tomorrow());

        when(authTokenUtil.calculateExpirationDate(any())).thenCallRealMethod();
        String token = authTokenFactory.create(userDetails);

        //When
        Boolean result = authTokenService.validate(token, userDetails);

        //Then
        assertThat(result).isFalse();
    }

    @Test
    public void shouldReturnTrueIfTokenCanBeRefreshed() {
        //Given
        when(clock.now()).thenReturn(DateUtil.now());
        when(authTokenExtractor.getIssuedAtDateFrom(anyString())).thenReturn(DateUtil.now());
        when(authTokenExtractor.getExpirationDateFrom(anyString())).thenReturn(DateUtil.tomorrow());
        Date lastPasswordReset = DateUtil.yesterday();

        String token = createToken();

        //When
        Boolean result = authTokenService.canBeRefreshed(token, lastPasswordReset);

        //Then
        assertThat(result).isTrue();
    }

    @Test
    public void shouldReturnFalseIfTokenCannotBeRefreshedWhenPasswordWasChanged() {
        //Given
        when(clock.now()).thenReturn(DateUtil.now());
        when(authTokenExtractor.getIssuedAtDateFrom(anyString())).thenReturn(DateUtil.now());
        when(authTokenExtractor.getExpirationDateFrom(anyString())).thenReturn(DateUtil.tomorrow());
        Date lastPasswordReset = new Date(DateUtil.now().getTime() + 3600);

        String token = createToken();

        //When
        Boolean result = authTokenService.canBeRefreshed(token, lastPasswordReset);

        //Then
        assertThat(result).isFalse();
    }

    @Test
    public void shouldReturnFalseIfTokenCannotBeRefreshedWhenIsExpired() {
        //Given
        when(clock.now()).thenReturn(DateUtil.now());
        when(authTokenExtractor.getIssuedAtDateFrom(anyString())).thenReturn(DateUtil.now());
        when(authTokenExtractor.getExpirationDateFrom(anyString())).thenReturn(DateUtil.yesterday());
        Date lastPasswordReset = DateUtil.yesterday();

        String token = createToken();

        //When
        Boolean result = authTokenService.canBeRefreshed(token, lastPasswordReset);

        //Then
        assertThat(result).isFalse();
    }

    @Test
    public void shouldReturnRefreshedToken() {
        //Given
        when(clock.now()).thenReturn(DateUtil.now(), DateUtil.tomorrow());

        when(authTokenUtil.calculateExpirationDate(any())).thenCallRealMethod();
        when(authTokenExtractor.getIssuedAtDateFrom(anyString())).thenCallRealMethod();
        when(authTokenExtractor.getExpirationDateFrom(anyString())).thenCallRealMethod();
        when(authTokenExtractor.getAllClaimsFrom(anyString())).thenCallRealMethod();
        String token = createToken();

        //When
        String result = authTokenService.refresh(token);

        //Then
        Date firstTokenCreationDate = authTokenExtractor.getIssuedAtDateFrom(token);
        Date resultTokenCreationDate = authTokenExtractor.getIssuedAtDateFrom(result);
        assertThat(firstTokenCreationDate).isBefore(resultTokenCreationDate);
    }


    private String createToken() {
        return authTokenFactory.create(new UserDetailsDummy(USERNAME));
    }
}
