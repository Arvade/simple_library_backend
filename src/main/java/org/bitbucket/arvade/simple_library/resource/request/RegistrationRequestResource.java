package org.bitbucket.arvade.simple_library.resource.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RegistrationRequestResource {

    @NotEmpty(message = "Username is missing")
    @Length(min = 3, max = 30, message = "Username length must be between 3 and 30")
    private String username;

    @NotEmpty(message = "Username is missing")
    @Length(min = 5, max = 50, message = "Password length must be between 5 and 50")
    private String password;

    @NotEmpty(message = "Email is missing")
    @Length(min = 4, max = 50, message = "Email length must be between 4 and 50")
    @Email(message = "Email is incorrect")
    private String email;

    @NotEmpty(message = "First name is missing")
    @Length(min = 3, max = 50, message = "First name length must be between 3 and 50")
    private String firstname;

    @NotEmpty(message = "Last name is missing")
    @Length(min = 3, max = 50, message = "First name length must be between 3 and 50")
    private String lastname;
}
