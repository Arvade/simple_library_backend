package org.bitbucket.arvade.simple_library.security.controller;

import org.bitbucket.arvade.simple_library.resource.request.RegistrationRequestResource;
import org.bitbucket.arvade.simple_library.security.exception.AuthenticationException;
import org.bitbucket.arvade.simple_library.security.factory.AuthTokenFactory;
import org.bitbucket.arvade.simple_library.security.model.AuthUser;
import org.bitbucket.arvade.simple_library.security.resource.AuthRequestResource;
import org.bitbucket.arvade.simple_library.security.resource.AuthResponseResource;
import org.bitbucket.arvade.simple_library.security.service.AuthTokenExtractor;
import org.bitbucket.arvade.simple_library.security.service.AuthTokenService;
import org.bitbucket.arvade.simple_library.service.UserService;
import org.bitbucket.arvade.simple_library.validator.RegistrationRequestResourceValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Optional;

@CrossOrigin(value = "*")
@RestController
public class AuthController {

    private RegistrationRequestResourceValidator registrationRequestValidator;
    private AuthenticationManager authenticationManager;
    private UserDetailsService userDetailsService;
    private AuthTokenExtractor authTokenExtractor;
    private AuthTokenFactory authTokenFactory;
    private AuthTokenService authTokenService;
    private UserService userService;

    @Value("${jwt.header}")
    private String tokenHeader;

    @Autowired
    public AuthController(RegistrationRequestResourceValidator registrationRequestValidator,
                          AuthenticationManager authenticationManager,
                          @Qualifier("authUserDetailsService") UserDetailsService userDetailsService,
                          AuthTokenExtractor authTokenExtractor,
                          AuthTokenFactory authTokenFactory,
                          AuthTokenService authTokenService,
                          UserService userService) {
        this.registrationRequestValidator = registrationRequestValidator;
        this.authenticationManager = authenticationManager;
        this.userDetailsService = userDetailsService;
        this.authTokenExtractor = authTokenExtractor;
        this.authTokenFactory = authTokenFactory;
        this.authTokenService = authTokenService;
        this.userService = userService;
    }

    @PostMapping(value = "${jwt.route.auth.path}")
    public ResponseEntity getAuthToken(@RequestBody @Valid AuthRequestResource authRequest) {
        String username = authRequest.getUsername();
        String password = authRequest.getPassword();

        authenticate(username, password);

        UserDetails userDetails = userDetailsService.loadUserByUsername(username);
        String token = authTokenFactory.create(userDetails);

        return ResponseEntity.ok(new AuthResponseResource(token));
    }

    @GetMapping(value = "${jwt.route.auth.refresh}")
    public ResponseEntity refreshAndGetAuthToken(HttpServletRequest request) {
        String authToken = Optional.ofNullable(request.getHeader(tokenHeader))
                .orElseThrow(AuthenticationException::new);

        final String token = authToken.substring(7);
        String username = authTokenExtractor.getUsernameFrom(token);
        AuthUser user = (AuthUser) userDetailsService.loadUserByUsername(username);

        if (authTokenService.canBeRefreshed(token, user.getLastPasswordResetDate())) {
            String refreshedToken = authTokenService.refresh(token);
            return ResponseEntity.ok(new AuthResponseResource(refreshedToken));
        } else {
            return ResponseEntity.badRequest().build();
        }
    }

    @PostMapping(value = "/register")
    public ResponseEntity register(@Valid @RequestBody RegistrationRequestResource resource) {
        userService.register(resource);
        return ResponseEntity.ok().build();
    }

    @InitBinder("registrationRequestResource")
    public void initRegistrationBinder(WebDataBinder binder) {
        binder.addValidators(registrationRequestValidator);
    }

    private void authenticate(String username, String password) {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new AuthenticationException("User is disabled.", e);
        } catch (BadCredentialsException e) {
            throw new AuthenticationException("Incorrect username or password.", e);
        }
    }
}
