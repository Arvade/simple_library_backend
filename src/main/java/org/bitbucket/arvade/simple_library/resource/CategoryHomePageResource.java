package org.bitbucket.arvade.simple_library.resource;

import lombok.Data;
import org.bitbucket.arvade.simple_library.resource.book.BookHomePageResource;

import java.util.List;

@Data
public class CategoryHomePageResource {

    private Long id;

    private String name;

    private List<BookHomePageResource> bookList;
}
