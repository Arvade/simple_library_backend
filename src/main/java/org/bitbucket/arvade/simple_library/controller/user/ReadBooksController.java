package org.bitbucket.arvade.simple_library.controller.user;

import org.bitbucket.arvade.simple_library.resource.book.BookHomePageResource;
import org.bitbucket.arvade.simple_library.service.ReadBookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(value = "*")
@RestController
@PreAuthorize("hasAnyRole('USER', 'ADMIN')")
public class ReadBooksController {

    private ReadBookService readBookService;

    @Autowired
    public ReadBooksController(ReadBookService readBookService) {
        this.readBookService = readBookService;
    }

    @GetMapping(value = "/app/books/readBooks")
    public ResponseEntity getReadBooksForCurrentUser() {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        List<BookHomePageResource> homePageResources = readBookService.getReadBooksForUser(username);
        return ResponseEntity.ok(homePageResources);
    }

    @GetMapping(value = "/app/books/markAsRead/{id}")
    public ResponseEntity setBookAsReadForCurrentUser(@PathVariable Long id) {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        readBookService.setBookAsReadForUser(id, username);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @DeleteMapping(value = "/app/books/markAsRead/{id}")
    public ResponseEntity setBookAsUnreadForCurrentUser(@PathVariable Long id) {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        readBookService.setBookAsUnreadForUser(id, username);
        return ResponseEntity.status(HttpStatus.OK).build();
    }
}