package org.bitbucket.arvade.simple_library.handler;

import org.bitbucket.arvade.simple_library.resource.ApiErrors;
import org.bitbucket.arvade.simple_library.resource.ApiFieldError;
import org.bitbucket.arvade.simple_library.resource.ApiGlobalError;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.stream.Collectors;

@ControllerAdvice
public class ApiValidationExceptionHandler extends ResponseEntityExceptionHandler {

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
            MethodArgumentNotValidException ex,
            HttpHeaders headers,
            HttpStatus status,
            WebRequest request) {
        ApiErrors apiErrors = new ApiErrors();
        BindingResult bindingResult = ex.getBindingResult();

        apiErrors.addAllFieldErrors(bindingResult.getFieldErrors()
                .stream()
                .map(fieldError -> new ApiFieldError(
                        fieldError.getField(),
                        fieldError.getDefaultMessage(),
                        fieldError.getRejectedValue()
                ))
                .collect(Collectors.toList()));

        apiErrors.addAllGlobalErrors(bindingResult.getGlobalErrors()
                .stream()
                .map(globalError -> new ApiGlobalError(globalError.getCode()))
                .collect(Collectors.toList()));

        return new ResponseEntity<>(apiErrors, HttpStatus.UNPROCESSABLE_ENTITY);
    }
}
