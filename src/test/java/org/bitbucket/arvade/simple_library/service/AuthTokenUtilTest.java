package org.bitbucket.arvade.simple_library.service;

import org.assertj.core.util.DateUtil;
import org.bitbucket.arvade.simple_library.security.service.AuthTokenUtil;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.Date;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class AuthTokenUtilTest {

    private static final Long EXPIRATION_ONE_HOUR = 3600L;

    private AuthTokenUtil authTokenUtil;

    @Before
    public void setup(){
        this.authTokenUtil = new AuthTokenUtil();
        ReflectionTestUtils.setField(authTokenUtil, "expiration", EXPIRATION_ONE_HOUR);
    }

    @Test
    public void shouldReturnExpirationDate(){
        //Given
        Date creationDate = DateUtil.now();
        Date expectedExpirationDate = new Date(creationDate.getTime() + EXPIRATION_ONE_HOUR * 1000);

        //When
        Date result = authTokenUtil.calculateExpirationDate(creationDate);

        //Then
        assertThat(result).isEqualToIgnoringSeconds(expectedExpirationDate);
    }
}
