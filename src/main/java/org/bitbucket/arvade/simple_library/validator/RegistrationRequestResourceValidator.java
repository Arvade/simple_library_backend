package org.bitbucket.arvade.simple_library.validator;


import org.bitbucket.arvade.simple_library.repository.UserRepository;
import org.bitbucket.arvade.simple_library.resource.request.RegistrationRequestResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class RegistrationRequestResourceValidator implements Validator {

    private UserRepository userRepository;

    @Autowired
    public RegistrationRequestResourceValidator(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return RegistrationRequestResource.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        RegistrationRequestResource registrationRequest = (RegistrationRequestResource) target;
        String username = registrationRequest.getUsername();
        String email = registrationRequest.getEmail();

        if (userRepository.findByUsername(username).isPresent()) {
            errors.reject("Username is already used.");
        }

        if (userRepository.findByEmail(email).isPresent()) {
            errors.reject("Email is already used");
        }
    }
}
