package org.bitbucket.arvade.simple_library.service.impl;

import org.bitbucket.arvade.simple_library.exception.NotFoundException;
import org.bitbucket.arvade.simple_library.model.Book;
import org.bitbucket.arvade.simple_library.model.ReadBook;
import org.bitbucket.arvade.simple_library.model.User;
import org.bitbucket.arvade.simple_library.repository.ReadBookRepository;
import org.bitbucket.arvade.simple_library.resource.book.BookHomePageResource;
import org.bitbucket.arvade.simple_library.service.BookService;
import org.bitbucket.arvade.simple_library.service.ReadBookService;
import org.bitbucket.arvade.simple_library.service.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ReadBookServiceImpl implements ReadBookService {

    private UserService userService;
    private BookService bookService;
    private ModelMapper modelMapper;
    private ReadBookRepository readBookRepository;

    @Autowired
    public ReadBookServiceImpl(UserService userService,
                               BookService bookService,
                               ModelMapper modelMapper,
                               ReadBookRepository readBookRepository) {
        this.userService = userService;
        this.bookService = bookService;
        this.modelMapper = modelMapper;
        this.readBookRepository = readBookRepository;
    }

    @Override
    public void setBookAsReadForUser(Book book, User user) {
        if (isBookSetAsReadForUser(book.getId(), user.getUsername())) return;
        ReadBook readBook = new ReadBook(book, user);
        readBookRepository.save(readBook);
    }


    @Override
    public void setBookAsUnreadForUser(Long bookId, String username) {
        ReadBook readBook = readBookRepository.findByBook_IdAndUser_Username(bookId, username)
                .orElseThrow(() -> new NotFoundException("No readBook record found for bookId " + bookId + " and username " + username));
        readBookRepository.delete(readBook);
    }

    @Override
    public void setBookAsReadForUser(Long bookId, String username) {
        Book book = bookService.findCompleteBookById(bookId)
                .map(bookResource -> modelMapper.map(bookResource, Book.class))
                .orElseThrow(() -> new NotFoundException("No book found for id " + bookId));
        User user = userService.findByUsername(username)
                .map(userInformationResource -> modelMapper.map(userInformationResource, User.class))
                .orElseThrow(() -> new NotFoundException("No user found for username " + username));
        setBookAsReadForUser(book, user);
    }

    @Override
    public boolean isBookSetAsReadForUser(Long bookId, String username) {
        return readBookRepository.existsByBook_IdAndUser_username(bookId, username);
    }

    @Override
    public List<BookHomePageResource> getReadBooksForUser(String username) {
        return readBookRepository.findByUser_Username(username).stream()
                .map(readBook -> modelMapper.map(readBook, BookHomePageResource.class))
                .collect(Collectors.toList());
    }
}