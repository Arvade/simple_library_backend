package org.bitbucket.arvade.simple_library.controller.user;

import org.bitbucket.arvade.simple_library.exception.NotFoundException;
import org.bitbucket.arvade.simple_library.resource.UserInformationResource;
import org.bitbucket.arvade.simple_library.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(value = "*")
@RestController
@PreAuthorize("hasAnyRole('USER','ADMIN')")
public class UserInformationController {

    private UserService userService;

    @Autowired
    public UserInformationController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping(value = "/user/{username}")
    public ResponseEntity getUserInformation(@PathVariable String username) {
        UserInformationResource userInformation = userService.getUserInformation(username)
                .orElseThrow(() -> new NotFoundException("No user found for username " + username));
        return ResponseEntity.ok(userInformation);
    }
}
