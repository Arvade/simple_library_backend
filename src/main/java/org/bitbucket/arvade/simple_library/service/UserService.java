package org.bitbucket.arvade.simple_library.service;

import org.bitbucket.arvade.simple_library.resource.AdminPanelUserEntryResource;
import org.bitbucket.arvade.simple_library.resource.request.RegistrationRequestResource;
import org.bitbucket.arvade.simple_library.resource.UserInformationResource;
import org.springframework.data.domain.Page;

import java.util.Optional;

public interface UserService {

    void register(RegistrationRequestResource registrationRequest);

    void update(Long userId, String fieldName, Object value);

    Optional<UserInformationResource> findByUsername(String username);

    Optional<UserInformationResource> findByEmail(String email);

    Optional<UserInformationResource> getUserInformation(String username);

    Page<AdminPanelUserEntryResource> findUserListPaginated(Integer page, Integer size);
}
