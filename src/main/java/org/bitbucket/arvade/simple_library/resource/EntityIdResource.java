package org.bitbucket.arvade.simple_library.resource;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EntityIdResource {

    @NotNull
    private Long id;
}
