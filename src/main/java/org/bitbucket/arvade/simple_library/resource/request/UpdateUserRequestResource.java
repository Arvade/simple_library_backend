package org.bitbucket.arvade.simple_library.resource.request;

import lombok.Data;

@Data
public class UpdateUserRequestResource {

    private Long id;

    private String field;

    private Object newValue;
}