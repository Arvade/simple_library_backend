package org.bitbucket.arvade.simple_library.controller.guest;

import org.bitbucket.arvade.simple_library.exception.NotFoundException;
import org.bitbucket.arvade.simple_library.resource.CategoryResource;
import org.bitbucket.arvade.simple_library.resource.book.BookHomePageResource;
import org.bitbucket.arvade.simple_library.resource.book.CompleteBookResource;
import org.bitbucket.arvade.simple_library.service.BookService;
import org.bitbucket.arvade.simple_library.service.CategoryService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@CrossOrigin("*")
@RestController
public class GuestBooksController {

    private BookService bookService;
    private ModelMapper modelMapper;
    private CategoryService categoryService;

    @Autowired
    public GuestBooksController(BookService bookService,
                                ModelMapper modelMapper,
                                CategoryService categoryService) {
        this.bookService = bookService;
        this.modelMapper = modelMapper;
        this.categoryService = categoryService;
    }

    @GetMapping(value = "/books/home")
    public ResponseEntity getBooksCover() {
        List<BookHomePageResource> homePageBookCovers = bookService.findAllBooksWithContent().stream()
                .map(completeBookResource -> modelMapper.map(completeBookResource, BookHomePageResource.class))
                .collect(Collectors.toList());

        return ResponseEntity.ok(homePageBookCovers);
    }

    @GetMapping(value = "/books")
    public ResponseEntity getBooks() {
        List<CompleteBookResource> bookList = bookService.findAllBooksWithContent();
        return ResponseEntity.status(HttpStatus.OK).body(bookList);
    }

    @GetMapping(value = "/books/{id}")
    public ResponseEntity getBookById(@PathVariable Long id) {
        CompleteBookResource book = bookService.findCompleteBookById(id)
                .orElseThrow(() -> new NotFoundException("No book found for id " + id));
        return ResponseEntity.ok(book);
    }

    @GetMapping(value = "/books/categories")
    public ResponseEntity<List<CategoryResource>> getAllCategories() {
        List<CategoryResource> categories = categoryService.findAll();
        return ResponseEntity.ok(categories);
    }
}