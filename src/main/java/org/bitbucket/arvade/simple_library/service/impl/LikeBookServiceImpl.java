package org.bitbucket.arvade.simple_library.service.impl;

import org.bitbucket.arvade.simple_library.exception.NotFoundException;
import org.bitbucket.arvade.simple_library.model.Book;
import org.bitbucket.arvade.simple_library.model.LikeBook;
import org.bitbucket.arvade.simple_library.model.User;
import org.bitbucket.arvade.simple_library.repository.LikeBookRepository;
import org.bitbucket.arvade.simple_library.resource.book.BookHomePageResource;
import org.bitbucket.arvade.simple_library.service.BookService;
import org.bitbucket.arvade.simple_library.service.LikeBookService;
import org.bitbucket.arvade.simple_library.service.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class LikeBookServiceImpl implements LikeBookService {

    private BookService bookService;
    private UserService userService;
    private ModelMapper modelMapper;
    private LikeBookRepository likeBookRepository;

    @Autowired
    public LikeBookServiceImpl(BookService bookService,
                               UserService userService,
                               ModelMapper modelMapper, LikeBookRepository likeBookRepository) {
        this.bookService = bookService;
        this.userService = userService;
        this.modelMapper = modelMapper;
        this.likeBookRepository = likeBookRepository;
    }

    @Override
    public boolean isBookLikedByUser(Long bookId, String username) {
        return likeBookRepository.existsByBook_IdAndUser_Username(bookId, username);
    }

    @Override
    public void likeBookForUser(Long bookId, String username) {
        Book book = bookService.findCompleteBookById(bookId)
                .map(bookResource -> modelMapper.map(bookResource, Book.class))
                .orElseThrow(() -> new NotFoundException("No book found for id " + bookId));

        User user = userService.findByUsername(username)
                .map(userInformationResource -> modelMapper.map(userInformationResource, User.class))
                .orElseThrow(() -> new NotFoundException("No user found for username " + username));

        likeBookForUser(book, user);
    }

    @Override
    public void likeBookForUser(Book book, User user) {
        likeBookRepository.save(new LikeBook(book, user));
    }

    @Override
    @Transactional
    public void unlikeBookForUser(Long bookId, String username) {
        likeBookRepository.deleteByBook_IdAndUser_Username(bookId, username);
    }

    @Override
    public List<BookHomePageResource> getAllLikedBooksForUser(String username) {
        return likeBookRepository.findAllByUser_Username(username).stream()
                .map(likeBook -> modelMapper.map(likeBook, BookHomePageResource.class))
                .collect(Collectors.toList());
    }
}
